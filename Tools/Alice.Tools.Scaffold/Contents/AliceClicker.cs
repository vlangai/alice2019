﻿using System;
using System.Collections.Generic;

namespace Alice.Tools.Scaffold.Contents
{
    public partial class AliceClicker
    {
        public long Id { get; set; }
        public long PlayerId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Property { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
