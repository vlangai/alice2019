﻿using System;
using System.Collections.Generic;

namespace Alice.Tools.Scaffold.Contents
{
    public partial class Player
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string NickName { get; set; }
    }
}
