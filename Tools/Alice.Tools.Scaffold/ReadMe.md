﻿# Scaffold 하는 방법

1. 시작프로그램을 현재 프로젝트로 설정한다.
2. 기본 프로젝트를 현재 프로젝트로 설정한다.
3. 전체 프로젝트에서 오류가 있으면 안된다.


```
PM> Scaffold-DbContext "server=localhost;port=3306;user=alice;password=alice;database=alice_auth" Pomelo.EntityFrameworkCore.MySql -OutputDir Auth -f
PM> Scaffold-DbContext "server=localhost;port=3306;user=alice;password=alice;database=alice_contents" Pomelo.EntityFrameworkCore.MySql -OutputDir Contents -f
```