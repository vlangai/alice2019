﻿namespace AliceClicker.Models {
    public interface IPack {

        int packetId { get; set; }
    }
}