﻿namespace AliceClicker.Models {

    public class DamagePack : ResponsePack {

        public DamagePack ( int packetId ) : base( packetId ) { }

        public float damage { get; set; }
        public int exp { get; set; }
        public int coin { get; set; }
    }
}
