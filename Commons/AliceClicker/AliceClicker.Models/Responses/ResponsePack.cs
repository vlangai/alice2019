﻿namespace AliceClicker.Models {

    public class ResponsePack : IPack {

        public ResponsePack ( int packetId ) {

            this.packetId = packetId;
        }

        public int packetId { get; set; }
        public int resultCode { get; set; }
        public string resultMessage { get; set; }
    }
}
