﻿namespace AliceClicker.Models {

    public class RequestPack : IPack {

        public int packetId { get; set; }
        public string method { get; set; }
    }
}
