﻿namespace AliceFPS {

    public struct ExitPack : ISyncPack {

        public int id { get; set; }

        public ExitPack ( string pack ) : this( pack.Split(',') ) { }
        public ExitPack ( string[] pack ) => this.id = int.Parse( pack[1] );
        public override string ToString () => $"{(int)Key.Exit:000},{this.id}";
    }
}
