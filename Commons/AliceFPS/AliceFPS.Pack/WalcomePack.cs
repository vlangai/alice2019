﻿namespace AliceFPS {

    public struct WalcomePack : ISyncPack {

        public int id { get; set; }

        public WalcomePack ( string pack ) : this( pack.Split(',') ) { }
        public WalcomePack ( string[] pack ) => this.id = int.Parse( pack[1] );
        public override string ToString () => $"{(int)Key.Walcome:000},{this.id}"; // ex) 001,1
    }
}
