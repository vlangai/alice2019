﻿namespace AliceFPS {

    public struct EnterChannelPack : ISyncPack {

        public int id { get; set; }

        public EnterChannelPack ( string pack ) : this( pack.Split(',') ) { }
        public EnterChannelPack ( string[] pack ) => this.id = int.Parse( pack[1] );
        public override string ToString () => $"{(int)Key.EnterChannel:000},{this.id}"; // ex) 001,1
    }
}
