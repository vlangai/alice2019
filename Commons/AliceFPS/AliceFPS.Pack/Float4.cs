﻿namespace AliceFPS {

    public struct Float4 {

        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float w { get; set; }


        public Float4 ( string pack ) : this( pack.Split(':') ) { }
        public Float4 ( string[] pack ) {

            this.x = float.Parse( pack[0] );
            this.y = float.Parse( pack[1] );
            this.z = float.Parse( pack[2] );
            this.w = float.Parse( pack[3] );
        }
        public override string ToString () => $"{this.x}:{this.y}:{this.z}:{this.w}"; // x:y:z:w
    }
}
