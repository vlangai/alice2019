﻿namespace AliceFPS {

    public struct SpawnPack : ISyncPack {

        public int id { get; set; }
        public Float3 pos { get; set; }
        public Float4 rot { get; set; }

        public SpawnPack ( string pack ) : this( pack.Split(',') ) { }
        public SpawnPack ( string[] pack ) {

            this.id = int.Parse( pack[1] );
            this.pos = new Float3( pack[2] );
            this.rot = new Float4( pack[3] );
        }
        public override string ToString () => $"{(int)Key.Spawn:000},{this.id},{this.pos},{this.rot}"; // key,id,pos,rot    ex) 002,1,1.0:2.0:3.0,1.0:2.0:3.0:4.0
    }
}
