﻿namespace AliceFPS {

    public interface ISyncPack {
        int id { get; set; }
    }
}