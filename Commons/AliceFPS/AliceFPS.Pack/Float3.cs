﻿namespace AliceFPS {

    public struct Float3 {

        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }


        public Float3 ( string pack ) : this( pack.Split(':') ) { }
        public Float3 ( string[] pack ) {

            this.x = float.Parse( pack[0] );
            this.y = float.Parse( pack[1] );
            this.z = float.Parse( pack[2] );
        }
        public override string ToString () => $"{this.x}:{this.y}:{this.z}"; // x:y:z
    }
}
