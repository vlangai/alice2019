﻿namespace AliceFPS {

    public enum Key : int {

        None,
        Walcome, // 1
        EnterChannel,
        Spawn,
        Transform,
        Exit
    }
}
