﻿namespace AliceFPS {

    public struct TransformPack : ISyncPack {

        public int      id      { get; set; }
        public Float3   pos     { get; set; }
        public Float4   rot     { get; set; }
        public Float3   scale   { get; set; }

        public TransformPack ( string pack ) : this( pack.Split(',') ) { }
        public TransformPack ( string[] pack ) {

            this.id     = int.Parse( pack[1] );
            this.pos    = new Float3( pack[2] );
            this.rot    = new Float4( pack[3] );
            this.scale  = new Float3( pack[4] );
        }
        public override string ToString () => $"{(int)Key.Transform:000},{this.id},{this.pos},{this.rot},{this.scale}"; // key,id,pos,rot,scale 
    }
}
