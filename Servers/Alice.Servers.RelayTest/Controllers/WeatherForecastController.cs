﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Alice.Servers.RelayTest.Controllers {

    [ApiController]
    [Route( "[controller]" )]
    public class WeatherForecastController : ControllerBase {

        public WeatherForecastController () {}

        [HttpGet]
        public IActionResult Get () => this.Ok( "Alice FPS Relay Test Service" );

   }
}
