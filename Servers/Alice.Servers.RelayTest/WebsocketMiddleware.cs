﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.RelayTest {

    public class WebsocketMiddleware {

        public WebsocketMiddleware ( RequestDelegate next, WebsocketService service ) {

            this.next = next;
            this.service = service;
        }

        public async Task Invoke ( HttpContext context ) {

            if( context.WebSockets.IsWebSocketRequest ) {

                await this.service.run( await context.WebSockets.AcceptWebSocketAsync() );
            }
            else {

                await this.next.Invoke( context );
            }

        }


        readonly RequestDelegate next;
        readonly WebsocketService service;

    }
}
