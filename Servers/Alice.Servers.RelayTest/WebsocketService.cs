﻿using Alice.Servers.Relay;
using AliceFPS;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Alice.Servers.RelayTest {


    public class WebsocketService {

        // -----------------------------------------------

        public WebsocketService ( RelayService relayService ) => this.relay = relayService;

        // -----------------------------------------------

        public async Task run ( WebSocket socket ) {

            var buffer = new byte[ 1024 * 4 ];
            var clientId = this.relay.addClient( this.send );
            this.sockets.TryAdd( clientId, socket );

            try {

                this.relay.connect( clientId );
                await this.send( clientId, new WalcomePack{ id=clientId }.ToString() );

                while( socket.State == WebSocketState.Open ) {

                    var result = await socket.ReceiveAsync( new ArraySegment<byte>( buffer ), CancellationToken.None );
                    await this.onRecv( clientId, result, buffer );
                }
            }
            catch {

                await this.close( clientId );
            }

        }
        private async Task onRecv ( int clientId, WebSocketReceiveResult result, byte[] buffer ) {


            if( result.MessageType == WebSocketMessageType.Text ) {

                var pack = Encoding.UTF8.GetString( buffer, 0, result.Count );
                //Console.WriteLine( $"recv : {pack}" );
                this.relay.recv( pack );

            }
            else
            if( result.MessageType == WebSocketMessageType.Close ) {

                await this.close( clientId );
            }
        }
        private async Task send ( int clientId, string pack ) {
        
            if( this.sockets.TryGetValue( clientId, out var socket ) ) {

                if( socket.State == WebSocketState.Open ) {

                    await socket.SendAsync( new ArraySegment<byte>( Encoding.UTF8.GetBytes( pack ) ), WebSocketMessageType.Text, true, CancellationToken.None );
                }
            }
        }
        private async Task close ( int clientId ) {

            Console.WriteLine( $"close : {clientId}" );
            this.sockets.TryRemove( clientId, out var socket );
            this.relay.disconnect( clientId );

            if( socket != null ) {

                if( socket.State == WebSocketState.Open ) {

                    await socket.CloseAsync( WebSocketCloseStatus.NormalClosure, "Closed by the server", CancellationToken.None );
                }
            }
        }

        // -----------------------------------------------


        readonly ConcurrentDictionary<int,WebSocket> sockets = new ConcurrentDictionary<int, WebSocket>();
        readonly RelayService relay;
    }
}
