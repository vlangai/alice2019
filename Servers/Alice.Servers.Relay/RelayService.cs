﻿namespace Alice.Servers.Relay {
    using AliceFPS;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class RelayService : IDisposable {

        public static RelayService Instance { get; private set; }

        public RelayService () {

            Instance = this;

            Task.Run( ()=> this.worker(), this.cancellation.Token );
        }

        // -------------------------------------------------------

        public int addClient( Func<int, string, Task> onSend ) {

            var client = new Client( this, ++MakeClientID, onSend );
            this.clients[ client.id ] = client;

            return client.id;
        }

        public void connect ( int clientId ) {

            if( this.clients .TryGetValue( clientId, out var client ) ) {

                client.connect();
            }
        }

        public void disconnect( int clientId ) {

            Console.WriteLine( $"disconnect : {clientId}" );
            this.clients.TryRemove( clientId, out _ );
            this.recv( new ExitPack { id = clientId }.ToString() );
        }

        public List<Client> getClients() => this.clients.Values.ToList();

        public void recv ( string pack ) => this.recvQueue.Enqueue( pack );

        // -------------------------------------------------------
        private async Task worker () {

            while( !this.cancellation.IsCancellationRequested ) {

                await Task.Delay(1);

                try {

                    if( this.recvQueue.TryDequeue( out var pack ) ) {

                        var p = this.parsePack( pack );

                        if( p.Item3 != Key.Transform ) {

                            Console.WriteLine( p );
                        }

                        if( p.Item1 ) {

                            foreach( var kv in this.clients ) {

                                await kv.Value.recv( p.Item3, p.Item4, p.Item5, pack );
                            }
                        }
                    }

                }
                catch ( Exception ex ) {

                    Console.WriteLine( ex.ToString() );
                }
            }
        }

        private (bool,string,Key,int,string[]) parsePack ( string pack  ) {

            var parameters = pack.Split(',');

            if( parameters.Length < 2 ) {

                return (false, $"error : Pack Size = {parameters.Length}", Key.None, -1, parameters );
            }
            if( !int.TryParse( parameters[0], out var keyInt ) ) {

                return (false, $"error : Unknown Key = {parameters[0]}", Key.None, -1, parameters);
            }
            var key = (Key)keyInt;

            if( !int.TryParse( parameters[1], out var senderId ) ) {

                return (false, $"error : Unknown Id = {parameters[1]}", Key.None, -1, parameters);
            }
            if( this.clients.TryGetValue( senderId, out var client ) ) {

                if( !client.validate( key ) ) {

                    return (false, $"error : Invalid Packet. key = {key}, id = {senderId}", Key.None, -1, parameters);
                }
            }

            return (true, "", key, senderId, parameters);

        }

        public void Dispose () {

            if( this.cancellation != null ) {

                this.cancellation.Cancel();
                this.cancellation = null;
            }

            this.clients.Clear();
        }

        // -------------------------------------------------------

        readonly ConcurrentDictionary<int,Client> clients = new ConcurrentDictionary<int, Client>();
        readonly ConcurrentQueue<string> recvQueue = new ConcurrentQueue<string>();
        CancellationTokenSource cancellation = new CancellationTokenSource();
        static int MakeClientID = 0;
    }
}
