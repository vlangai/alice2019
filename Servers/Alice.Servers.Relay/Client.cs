﻿namespace Alice.Servers.Relay {

    using AliceFPS;
    using System;
    using System.Threading.Tasks;


    public class Client : IDisposable {

        // --------------------------------------------------

        public int      id          { get; private set; }
        public bool     connected   { get; private set; }
        public bool     isObserver  { get; private set; }
        public Float3   pos         { get; private set; }
        public Float4   rot         { get; private set; }
        public Float3   scale       { get; private set; }

        // --------------------------------------------------

        public void connect () => this.connected = true;

        public Client ( RelayService service, int id , Func<int,string,Task> onSend ) {

            this.service    = service;
            this.id         = id;
            this.onSend     = onSend;
            this.isObserver = true;
        }


        public async Task recv ( Key key, int senderId, string[] parameters, string pack ) {

            if( !this.connected ) return;

            if( senderId == this.id ) {

                switch( key ) {

                case Key.EnterChannel:
                    await this.onEnterChannel( new EnterChannelPack( parameters ) );
                    break;
                case Key.Spawn:
                    this.onSpawn( new SpawnPack( parameters ) );
                    break;
                case Key.Transform:
                    this.onTtransform( new TransformPack( parameters ) );
                    break;
                }
            }
            else {

                if( key == Key.Walcome || key == Key.EnterChannel ) return;
            }

            await this.onSend?.Invoke( this.id, pack );
        }

        public bool validate ( Key key ) {

            switch( key ) {
            case Key.None:
                return false;
            case Key.Walcome:
                if( !this.isObserver ) return false;
                break;
            case Key.EnterChannel:
                if( !this.isObserver ) return false;
                break;
            case Key.Spawn:
                if( !this.isObserver ) return false;
                break;
            case Key.Transform:
                if( this.isObserver ) return false;
                break;
            case Key.Exit:
                break;
            }

            return true;
        }

        // --------------------------------------------------

        private async Task onEnterChannel ( EnterChannelPack pack ) {

            foreach( var client in this.service.getClients() ) {

                if( client == this ) continue;
                if( client.isObserver ) continue;
                await this.onSend?.Invoke( this.id, new SpawnPack { id = client.id, pos = client.pos, rot = client.rot }.ToString() );
            }

            await Task.CompletedTask;
        }
        private void onSpawn ( SpawnPack pack ) {

            this.pos = pack.pos;
            this.rot = pack.rot;
            this.isObserver = false;
        }
        private void onTtransform ( TransformPack pack ) {

            this.pos    = pack.pos;
            this.rot    = pack.rot;
            this.scale  = pack.scale;
        }

        public void Dispose () { 

            if( this.service != null ) {

                this.service.disconnect( this.id );
                this.service = null;
            }
        }

        // --------------------------------------------------


        RelayService service;
        Func<int,string,Task> onSend;
    }
}