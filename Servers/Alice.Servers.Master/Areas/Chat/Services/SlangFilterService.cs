﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Services {
    public static class SlangFilterService {

        private static Dictionary<string,string> SlangDic = new Dictionary<string, string>{

            ["fuck you"]="OMG",
            ["holy shit"]="Jesus",
        };

        public static string Filtering( string message ) {

            foreach( var kv in SlangDic ) {

                message = message.Replace( kv.Key, kv.Value, StringComparison.CurrentCultureIgnoreCase );
            }

            return message;
        }
    }
}
