﻿using Alice.Servers.Master.Contents.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Services {
    public class ChatChannel {

        public delegate void OnAddMessage( string message );

        public OnAddMessage onAddMessage { get; set; }

        public string name { get; set; }

        public bool canDestroy { get; set; }

        public ChatChannel( string name, bool canDestroy = true ) {

            this.name = name;
            this.canDestroy = canDestroy;
        }

        /// <summary> 전체 채팅 메시지 목록 </summary>
        public List<string> messages { get; private set; } = new List<string>(); // 초기화

        /// <summary> 플레이어 목록 </summary>
        public List<Player> players { get; set; } = new List<Player>();

        /// <summary> 메시지 추가 </summary>
        public void addMessage( Player sender, string message ) {

            if( this.isInRoom( sender ) ) {

                message = $"{sender.nickname} : {message}";
                this.messages.Add( message );
            }
            else {

                this.messages.Add( $"{message}" );
            }

            this.onAddMessage?.Invoke( message );
        }

        /// <summary> 플레이어 입장 </summary>
        public void enterPlayer( Player player, OnAddMessage onAddMessage ) {

            if( !this.players.Contains( player ) ) {

                var isFirst = !this.players.Any( x => x.nickname == player.nickname );

                this.players.Add( player );
                this.onAddMessage += onAddMessage;

                if( isFirst ) {

                    this.addMessage( null, $"'{player.nickname}' has entered the room." );
                }
            }
        }

        /// <summary> 플레이어 퇴장 </summary>
        public void exitPlayer( Player player, OnAddMessage onAddMessage ) {

            this.players.Remove( player );
            this.onAddMessage -= onAddMessage;

            var isLast = !this.players.Any( x => x.nickname == player.nickname );
            if( isLast ) {

                this.addMessage( null, $"'{player.nickname}' has left the room." );
            }
        }

        /// <summary> 플레이어가 입장되어 있는가? </summary>
        public bool isInRoom( Player player ) => this.players.Contains( player );
    }
}
