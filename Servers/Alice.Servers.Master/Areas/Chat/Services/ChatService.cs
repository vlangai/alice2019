﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alice.Servers.Master.Contents.Players;

namespace Alice.Servers.Master.Areas.Chat.Services {
    public class ChatService {


        public static ChatService Instance;   // singleton

        public ChatService() {

            Instance = this;
            this.makeSeedData();
        }

        /// <summary> 시드 데이터 생성 </summary>
        private void makeSeedData() {

            this.channels.Add( "Real world", new ChatChannel( "Real world", false ) );
            this.channels.Add( "Wonderland", new ChatChannel( "Wonderland", false ) );
        }

        /// <summary> 플레이어 입장 </summary>
        public void enterPlayer( string channelName, Player player, ChatChannel.OnAddMessage onAddMessage ) {

            var channel = this.getChannel( channelName, player );
            if( channel == null ) {

                if( this.channels.TryGetValue( channelName, out channel ) == false ) {

                    // 채널 생성
                    channel = new ChatChannel( channelName );
                    this.channels.Add( channelName, channel );

                }
            }
            channel.enterPlayer( player, onAddMessage );
        }

        /// <summary> 플레이어 퇴장 </summary>
        public void exitPlayer( string channelName, Player player, ChatChannel.OnAddMessage onAddMessage ) {

            var channel = this.getChannel( channelName, player );
            if( channel != null ) {

                channel.exitPlayer( player, onAddMessage );

                if( channel.players.Count == 0 && channel.canDestroy )
                    this.channels.Remove( channelName );
            }
        }

        /// <summary> 메시지 추가 </summary>
        public void addMessage( string channelName, Player sender, string message ) {

            var channel = this.getChannel( channelName, sender );
            if( channel != null )
                channel.addMessage( sender, message );
        }


        /// <summary> 모든 채널 얻기 </summary>
        public List<ChatChannel> getChannels() => this.channels.Values.ToList();

        /// <summary> 플레이어가 채널에 입장되어 있는가? </summary>
        public bool IsInChannel( string channelName, Player player ) =>

            this.getChannel( channelName, player ) != null;

        /// <summary> 플레이어가 입장되어 있는 채널 얻기 </summary>
        public ChatChannel getChannel( string channelName, Player player ) {

            if( this.channels.TryGetValue( channelName, out var channel ) ) {

                if( channel.isInRoom( player ) )
                    return channel;
            }

            return null;

        }

        public bool existChannel( string channelName ) => this.channels.ContainsKey( channelName );


        // 모든 채널 목록
        private Dictionary<string, ChatChannel> channels { get; set; } = new Dictionary<string, ChatChannel>();
    }
}
