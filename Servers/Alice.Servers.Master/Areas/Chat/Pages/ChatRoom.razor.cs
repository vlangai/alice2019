﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Pages {
    public partial class ChatRoom {

        [Parameter]
        public Chat.ChannelInput channelInput { get; set; }
        [Parameter]
        public Chat.MessageInput messageInput { get; set; }
        [Parameter]
        public List<string> messages { get; set; }
        [Parameter]
        public EventCallback<EditContext> onValidSubmit { get; set; }
    }
}
