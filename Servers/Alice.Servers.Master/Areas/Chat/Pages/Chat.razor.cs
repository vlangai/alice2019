﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Pages {
    public partial class Chat {

        /// <summary> 초기화 함수 </summary>
        protected override async Task OnInitializedAsync() {

            await this.loadNickname();

            var channels = this.service.getChannels();
            if( channels.Count > 0 ) {
                this.channelInput.channelName = channels[0].name;
            }
            await Task.CompletedTask;
        }

        protected void onValidSubmitChannel() {

            this.enterChannel();
        }

        protected void onValidSubmitMessage() {

            this.addMessage();
        }
    }
}
