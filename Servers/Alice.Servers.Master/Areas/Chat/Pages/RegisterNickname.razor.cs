﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Pages {

    public partial class RegisterNickname {


        [Parameter]
        public string errorMessage { get; set; } = "";

        [Parameter]
        public Action<string> onRegisterNickname { get; set; }

        public Input input { get; set; } = new Input();

        public void onValidSubmit() => this.onRegisterNickname?.Invoke( this.input.nickName );


        public class Input {

            [DisplayName( "Nick Name" )]
            [MinLength( 2 ), MaxLength( 20 )]
            public string nickName { get; set; } = "";
        }

    }
}
