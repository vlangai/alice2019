﻿using Alice.Servers.Master.Areas.Chat.Services;
using Alice.Servers.Master.Contents.Players;
using Alice.Servers.Master.Data;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Pages {

    public partial class Chat : IDisposable {

        [Inject] IHttpContextAccessor httpContextAccessor { get; set; }
        [Inject] ContentsDbContext contentsDbContext { get; set; }


        protected ChannelInput channelInput { get; set; } = new ChannelInput();
        protected MessageInput messageInput {get; set;} = new MessageInput();
        protected List<string> messages { get; set; } = new List<string>();

        protected string nickName { get; set; } = "";

        public bool hasNickname() => this.nickName != "";
        public bool isInChannel() => this.service.IsInChannel( this.channelInput.channelName, this.my );


        [Inject]
        private ChatService service { get; set; }
        [Inject]
        private IJSRuntime js { get; set; }
        private Player my { get; set; }

        public string errorMessage { get; private set; } = "";

        private async Task loadNickname() {

            var userId = this.httpContextAccessor.HttpContext.User.FindFirstValue( ClaimTypes.NameIdentifier );
            this.my = await this.contentsDbContext.player.FirstOrDefaultAsync( x=>x.userId == userId );
            if( this.my != null ) {

                this.nickName = this.my.nickname;
            }
        }

        public void registerNickname( string nickName ) {

            this.errorMessage = "";

            if( this.my == null ) {

                if( this.contentsDbContext.player.Any( x=>x.nickname == nickName ) ) {

                    this.errorMessage = $"'{nickName}' is already registered.";
                    this.render();
                    return;
                }

                var userId = this.httpContextAccessor.HttpContext.User.FindFirstValue( ClaimTypes.NameIdentifier );

                this.my = new Player {
                    userId = userId,
                    nickname = nickName,
                };

                this.contentsDbContext.Add( this.my );
            }
            else {

                this.contentsDbContext.Update( this.my );
            }

            this.contentsDbContext.SaveChanges();

            this.nickName = nickName;
            this.render();
        }


        public bool existChannel( string channelName ) => this.service.existChannel( channelName );

        protected List<string> getChannelNames() => this.service.getChannels().Select(x=>x.name).ToList();

        private void enterChannel() {

            this.service.enterPlayer( this.channelInput.channelName, this.my, this.onAddMessage );
            this.messageInput.message = "";
        }
        private void exitChannel() {

            if( this.my != null ) {

                this.service?.exitPlayer( this.channelInput.channelName, this.my, this.onAddMessage );
                this.my = null;
            }
        }
        public void addMessage() {

            //Console.WriteLine( this.messageInput.message );

            var message = SlangFilterService.Filtering( this.messageInput.message );  //Console.WriteLine( message );

            this.service.addMessage( this.channelInput.channelName, this.my, message );
            this.messageInput.message = "";
        }
        private void onAddMessage( string message ) {

            this.messages.Add( message );
            this.render();
        }


        public void render() {

            Task.Run( async () => {

                await this.InvokeAsync( () => { this.StateHasChanged(); } );
                this.onScroll();

            } );
        }
        protected void onScroll() {

            this.js.InvokeVoidAsync( "chat.scrollToBottom" );
        }
        public void Dispose() {

            this.exitChannel();
        }
    }
}
