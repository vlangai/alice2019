﻿using Alice.Servers.Master.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Alice.Servers.Master.Areas.Chat.Pages {

    public partial class Chat {

        public class ChannelInput {

            public string selectedChannelOption { get; set; } = "";

            [DisplayName( "Channel Name" )]
            [UniqueCnannelName]
            [MinLength( 2 ), MaxLength( 20 )]
            public string channelName { get; set; } = "";
        }

        public class MessageInput {

            [DisplayName( "MESSAGE" )]
            [MinLength( 2 ), MaxLength( 256 )]
            public string message { get; set; } = "";
        }

    }
}
