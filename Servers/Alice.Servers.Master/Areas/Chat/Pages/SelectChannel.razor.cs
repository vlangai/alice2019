﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.Chat.Pages {
    public partial class SelectChannel {

        [Parameter]
        public RenderFragment ChildContent { get; set; }
        [Parameter]
        public Chat.ChannelInput channelInput { get; set; }

        [Parameter]
        public EventCallback<EditContext> onValidSubmit { get; set; }
        [Parameter]
        public Func<List<string>> getChannelNames { get; set; }


        protected void onChangeSelectChannel( ChangeEventArgs args ) {

            this.channelInput.selectedChannelOption = args.Value.ToString();
            this.channelInput.channelName = this.channelInput.selectedChannelOption;
        }
    }
}
