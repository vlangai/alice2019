﻿using Alice.Servers.Master.Areas.Chat.Services;
using System.ComponentModel.DataAnnotations;

namespace Alice.Servers.Master.Areas.Chat.Pages {

    public partial class Chat {

        public class UniqueCnannelNameAttribute : ValidationAttribute {

            public UniqueCnannelNameAttribute() : base( "{0} : This channel already exists." ) {

            }

            public override bool IsValid( object value ) => this.success;

            protected override ValidationResult IsValid( object value, ValidationContext validationContext ) {

                this.success = true;

                var model = validationContext.ObjectInstance as ChannelInput;
                if( model != null ) {

                    if( model.selectedChannelOption == "New Channel" ) {

                        if( ChatService.Instance.existChannel( model.channelName ) ) {

                            this.success = false;
                        }
                    }
                }

                return base.IsValid( value, validationContext );
            }


            private bool success;
        }
    }
}
