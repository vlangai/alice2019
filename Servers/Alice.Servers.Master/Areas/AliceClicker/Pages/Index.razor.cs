﻿using Alice.Helpers;
using Alice.Servers.Master.Areas.AliceClicker.Helpers;
using Alice.Servers.Master.Contents.Players;
using Alice.Servers.Master.Data;
using AliceClicker.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.JSInterop;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AliceClickerData = Alice.Servers.Master.Areas.AliceClicker.Data.AliceClicker;

namespace Alice.Servers.Master.Areas.AliceClicker.Pages {
    public partial class Index : IDisposable {

        [Inject] AuthenticationStateProvider auth { get; set; }
        [Inject] NavigationManager nav { get; set; }
        [Inject] IJSRuntime js { get; set; }
        [Inject] ContentsDbContext db {get; set; }

        public DbSet<AliceClickerData> dataTable { get; set; }
        public long playerId { get; set; }
        public string playToken { get; set; }

        static readonly Dictionary<string,Index> Users = new Dictionary<string, Index>();
        readonly ConcurrentQueue<string> requestQueue = new ConcurrentQueue<string>();
        readonly CancellationTokenSource cancelation = new CancellationTokenSource();

        // -------------------------------------------------------

        protected override async Task OnInitializedAsync () {

            var authState = await this.auth.GetAuthenticationStateAsync();
            var user = authState.User;
            var userId = user.FindFirstValue( ClaimTypes.NameIdentifier );
            if( userId == null ) {

                this.nav.NavigateTo( "/", true );
                return;
            }

            var player = await this.db.player.FirstOrDefaultAsync( x => x.userId == userId );
            if( player == null ) { // create player

                player = new Player{  nickname =  Guid.NewGuid().ToString(), userId = userId };
                this.db.player.Add( player );
                await this.db.SaveChangesAsync();
                player = await this.db.player.FirstOrDefaultAsync( x => x.userId == userId );
            }

            if( player == null ) {

                this.nav.NavigateTo( "/", true );
                return;
            }

            this.dataTable = this.db.Set<AliceClickerData>();
            this.playerId = player.id;
            this.playToken = Guid.NewGuid().ToString();
            Users[ this.playToken ] = this;

            await Task.Run( () => this.worker(), this.cancelation.Token );
        }

        protected override async Task OnAfterRenderAsync( bool firstRender ) {

            if( firstRender ) {

                while( this.playToken == null ) {

                    await Task.Delay(1);
                }

                await this.js.InvokeVoidAsync( "AliceClicker.run", this.playToken );
            }
        }

        private async Task worker () {

            while( !this.cancelation.IsCancellationRequested ) {

                await Task.Delay( 1 );

                try {

                    if( this.requestQueue.TryDequeue( out var inPacket ) ) {

                        var reqPack = inPacket.unpack<RequestPack>();

                        var outPacket = new ResponsePack( 0 ){ resultCode=-1,  resultMessage = "error" }.pack();

                        switch( reqPack.method ) {

                        case nameof( attack ): outPacket = await this.attack( inPacket ); break;
                        case nameof( skill ): outPacket = await this.skill( inPacket ); break;

                        }

                        await this.js.InvokeVoidAsync( "AliceClicker.response", outPacket );
                    }

                }
                catch( Exception ex ) {

                    var outPacket = new ResponsePack( 0 ) { resultCode=-1,  resultMessage = ex.Message }.pack();
                    await this.js.InvokeVoidAsync( "AliceClicker.response", outPacket );

                }

            }
        }

        // -------------------------------------------------------

        [JSInvokable]
        public static async Task AliceClicker_Request ( string playToken, string inPacket ) {

            Console.WriteLine( inPacket );
            if( Users.TryGetValue( playToken, out var user ) ) {

                user.requestQueue.Enqueue( inPacket );
            }
            await Task.CompletedTask;
        }

        // -------------------------------------------------------


        private async Task<string> attack( string packet ) {

            var attackPack = packet.unpack<AttackPack>();

            var defence = 1.5f;

            var damagePack = new DamagePack( attackPack.packetId ) {

                resultCode = 0,
                resultMessage = "",
                damage = attackPack.power - defence,
                exp = new Random().Next(10,20),
                coin = new Random().Next(1,10),
            };

            await this.addValue( 1, "exp", damagePack.exp );
            await this.addValue( 1, "coin", damagePack.coin );

            await this.db.SaveChangesAsync();

            return damagePack.pack();
        }

        private async Task<string> skill ( string packet ) {

            var skillPack = packet.unpack<SkillPack>();

            var resistance = 0.5f;
            var skillPower = skillPack.id * 1.5f;

            var damagePack = new DamagePack( skillPack.packetId ) {

                resultCode = 0,
                resultMessage = "",
                damage = skillPower - resistance,
                exp = (int)(new Random().Next(10,20) * skillPower ),
                coin = (int)(new Random().Next(1,10) * skillPower ),
            };

            await this.addValue( 1, "exp", damagePack.exp );
            await this.addValue( 1, "coin", damagePack.coin );

            await this.db.SaveChangesAsync();

            return damagePack.pack();
        }

        // -------------------------------------------------------

        private async Task addValue<T>( int type, string name, T value ) {

            var row = await this.dataTable.FirstOrDefaultAsync( x => x.playerId == this.playerId &&  x.type == type && x.name == name );
            if( row == null ) {

                row = new AliceClickerData{ playerId = this.playerId, type = type, name = name };
                this.dataTable.Add( row );
            }
            else {

                this.db.Update( row );
            }

            row.value = row.value.plus( value );
            row.updateDate = DateTime.Now;
        }

        public void Dispose () {
        
            if( this.playToken != null ) {

                if( Users.ContainsKey( this.playToken ) ) {

                    Users.Remove( this.playToken );
                }
            }

            this.cancelation.Cancel();
        }
    }
}
