﻿using Alice.Servers.Master.Contents.Players;
using Microsoft.EntityFrameworkCore;

namespace Alice.Servers.Master.Areas.AliceClicker.Data {
    public static class AliceClickerBuilder {

        public static void Build ( ModelBuilder builder ) {

            var entity = builder.Entity<AliceClicker>();

            entity.HasIndex( e => e.playerId )
                .HasName( "playerId" );

            entity.HasIndex( e => new { e.playerId, e.type } )
                .HasName( "playerId_type" );

            entity.HasIndex( e => new { e.playerId, e.type, e.name } )
                .HasName( "playerId_type_name" )
                .IsUnique();

            entity.Property( e => e.id )
                .HasColumnName( "id" )
                .HasColumnType( "bigint(20)" );

            entity.Property( e => e.createDate )
                .HasColumnName( "createDate" )
                .HasColumnType( "datetime" )
                .HasDefaultValueSql( "'current_timestamp()'" );

            entity.Property( e => e.name )
                .IsRequired()
                .HasColumnName( "name" )
                .HasColumnType( "varchar(50)" );

            entity.Property( e => e.playerId )
                .HasColumnName( "playerId" )
                .HasColumnType( "bigint(20)" );

            entity.Property( e => e.property )
                .HasColumnName( "property" )
                .HasColumnType( "longtext" );

            entity.Property( e => e.type )
                .HasColumnName( "type" )
                .HasColumnType( "int(11)" );

            entity.Property( e => e.updateDate )
                .HasColumnName( "updateDate" )
                .HasColumnType( "datetime" );

            entity.Property( e => e.value )
                .IsRequired()
                .HasColumnName( "value" )
                .HasColumnType( "varchar(256)" );
        }

    }
}
