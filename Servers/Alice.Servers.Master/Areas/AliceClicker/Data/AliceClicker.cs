﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.AliceClicker.Data {
    public class AliceClicker {

        public long id { get; set; }
        public long playerId { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string property { get; set; }
        public DateTime? createDate { get; set; }
        public DateTime updateDate { get; set; }
    }
}
