﻿using Alice.Servers.Relay;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Areas.AliceFPS.Pages {

    public partial class Index : ComponentBase, IDisposable {

        [Inject] IJSRuntime js { get; set; }
        [Inject] RelayService service { get; set; }


        protected override async Task OnAfterRenderAsync( bool firstRender ) {

            if( firstRender ) {

                this.clientId = service.addClient( this.onSend );
                await this.js.InvokeVoidAsync( "AliceFPS.run", this.clientId );
            }
        }

        [JSInvokable]
        public static async Task AliceFPS_Connect ( int clientId ) {

            RelayService.Instance.connect( clientId );
            await Task.CompletedTask;
        }

        [JSInvokable]
        public static async Task AliceFPS_Disconnect ( int clientId ) {

            RelayService.Instance.disconnect( clientId );
            await Task.CompletedTask;
        }

        [JSInvokable]
        public static async Task AliceFPS_Recv ( string pack ) {

            RelayService.Instance.recv( pack );
            await Task.CompletedTask;
        }

        private async Task onSend ( int clientId, string pack ) => await this.js.InvokeVoidAsync( "AliceFPS.recv", pack );

        public void Dispose () {

            if( this.clientId != -1 ) {

                this.service.disconnect( this.clientId );
            }
        }


        private int clientId = -1;
    }
}
