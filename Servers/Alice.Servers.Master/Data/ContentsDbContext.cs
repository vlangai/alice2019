﻿using Alice.Servers.Master.Areas.AliceClicker.Data;
using Alice.Servers.Master.Contents.Players;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Data {
    public class ContentsDbContext : DbContext {

        public ContentsDbContext( DbContextOptions<ContentsDbContext> options ) : base( options ) {

        }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) {

            base.OnModelCreating( modelBuilder );


            PlayerBuilder.Build( modelBuilder );
            AliceClickerBuilder.Build( modelBuilder );

        }


        public virtual DbSet<Player> player { get; set; }
        //public virtual DbSet<AliceClicker> aliceClicker { get; set; }
    }
}
