﻿using Alice.Servers.Master.Contents.Players;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Data {
    public class AuthDbContext : IdentityDbContext {

        public AuthDbContext( DbContextOptions<AuthDbContext> options ) : base( options ) {

        }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) {

            base.OnModelCreating( modelBuilder );
        }
    }
}
