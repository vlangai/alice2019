﻿using System.ComponentModel;
using System.Reflection;

namespace Alice.Servers.Master.Helpers {
    public static class DataAnnotationsHelper {

        /// <summary> Get display name of property </summary>
        //public static string displayName( this object self, string propertyName ) {
        //
        //    var value = self.GetType().GetProperty(propertyName)?.GetCustomAttribute( typeof(DisplayAttribute), true ) as DisplayAttribute;
        //    return value?.Name ?? propertyName;
        //}
        public static string displayName( this object self, string propertyName ) {

            var value = self.GetType().GetProperty(propertyName)?.GetCustomAttribute( typeof(DisplayNameAttribute), true ) as DisplayNameAttribute;
            return value?.DisplayName ?? propertyName;
        }

    }
}
