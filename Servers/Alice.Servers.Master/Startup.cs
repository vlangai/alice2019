using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Alice.Servers.Master.Data;
using Alice.Servers.Master.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Components.Authorization;
using Alice.Servers.Master.Areas.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Alice.Servers.Master.Areas.Chat.Services;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using Alice.Servers.Relay;

namespace Alice.Servers.Naster {

    /// <summary> 어플리케이션 설정 클래스 </summary>
    public class Startup {

        /// <summary> 생성자 </summary>

        public Startup( IConfiguration configuration ) {
            Configuration = configuration;
        }

        /// <summary> appsettings.json 관리   </summary>

        public IConfiguration Configuration { get; }

        /// <summary> 각종 서비스 등록 </summary>
        public void ConfigureServices( IServiceCollection services ) {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<CounterService>();
            services.AddSingleton<ChatService>();
            services.AddSingleton<RelayService>();
            services.AddDbContextPool<AuthDbContext>( options => options.UseMySql( this.Configuration.GetConnectionString( "Auth" ) ) );
            services.AddDbContextPool<ContentsDbContext>( options => options.UseMySql( this.Configuration.GetConnectionString( "Contents" ) ) );

            services.AddDefaultIdentity<IdentityUser>( options => { 
                    //options.SignIn.RequireConfirmedEmail=true;
                 } )
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();


            //services.AddTransient<IEmailSender,EmailSender>();
            //services.Configure<EmailSetttings>( this.Configuration.GetSection( nameof(EmailSetttings) ) );

            services.AddAuthentication()
                    .AddFacebook( options => { 
                        options.AppId       = this.Configuration["Authentication:Facebook:AppId"];
                        options.AppSecret   = this.Configuration["Authentication:Facebook:AppSecret"];
                    } )
                    .AddGoogle( options=> {
                        options.ClientId        = this.Configuration["Authentication:Google:ClientId"];
                        options.ClientSecret    = this.Configuration["Authentication:Google:ClientSecret"];
                    } );
        }

        /// <summary> 각종 서비스 설정 </summary>
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {

            if( env.IsDevelopment() ) {

                app.UseDeveloperExceptionPage();
            }
            else {

                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler( "/Error" );
                //app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".unityweb"] = "application/octet-stream";
            app.UseStaticFiles( new StaticFileOptions{ 
                FileProvider = new PhysicalFileProvider( Path.Combine( Directory.GetCurrentDirectory(), "wwwroot" ) ),
                ContentTypeProvider = provider
            } );

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints( endpoints => {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage( "/_Host" );
            } );
        }
    }
}
