﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Services {

    public class EmailSender : IEmailSender {

        public EmailSender( IOptions<EmailSetttings> emailSetttings ) {

            this.emailSetttings = emailSetttings.Value;
        }


        public async Task SendEmailAsync( string email, string subject, string htmlMessage ) {


            var from = new MailAddress( this.emailSetttings.sender, this.emailSetttings.serverName );
            var to = new MailAddress( email );

            var message = new MailMessage( from, to ) {
                Subject = subject,
                SubjectEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Body = htmlMessage,
                BodyEncoding = Encoding.UTF8
            };


            using( var client = new SmtpClient() ) {

                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential( this.emailSetttings.sender, this.emailSetttings.password );
                client.EnableSsl = true;
                client.Host = this.emailSetttings.server;
                client.Port = this.emailSetttings.port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.SendCompleted += new SendCompletedEventHandler( this.onSendComplete );
                this.sendState = "send";
                client.SendAsync( message, email );

                // waiting...

                while( this.sendState == "send" ) {

                    await Task.Delay(10);
                }

            }

        }

        private void onSendComplete( object sender, AsyncCompletedEventArgs e ) {

            var userEmail = (string) e.UserState;

            if( e.Cancelled ) {

                this.sendState = "canceled";
                Console.WriteLine( $"send email to ({userEmail})" );
            }
            if( e.Error != null ) {

                this.sendState = "error";
                Console.WriteLine( $"send email to ({userEmail}) > error : {e.Error}" );
            }
            else {

                this.sendState = "sent";
                Console.WriteLine( $"send email to ({userEmail})" );
            }
        }


        private string sendState = "";


        private EmailSetttings emailSetttings;
    }

    public class EmailSetttings {

        public string   server        { get; set; }
        public int      port          { get; set; }
        public string   sender        { get; set; }
        public string   serverName    { get; set; }
        public string   password      { get; set; }
    }
}
