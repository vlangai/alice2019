﻿AliceClicker = {

    playToken: String,

    run: function ( playToken ) {

        this.playToken = playToken;

        if ( unityInstance !== undefined ) {

            unityInstance.Quit( function () {

                unityInstance = null;
                unityInstance = UnityLoader.instantiate( "unityContainer", "unity/AliceClicker/Build/AliceClicker.json", { onProgress: UnityProgress } );

            } );
        }
        else {

            unityInstance = UnityLoader.instantiate( "unityContainer", "unity/AliceClicker/Build/AliceClicker.json", { onProgress: UnityProgress } );
        }

    },

    request: function ( inPacket ) {

        console.log( "request : " + inPacket );
        DotNet.invokeMethodAsync( "Alice.Servers.Master", "AliceClicker_Request", this.playToken, inPacket );
    },


    response: function ( outPacket ) {

        console.log( "response", outPacket );
        unityInstance.SendMessage( "WebGLHelper", "onResult", outPacket );
    },

    notice: function ( outPacket ) {

        console.log( "notice", outPacket );
        unityInstance.SendMessage( "WebGLHelper", "onNotice", outPacket );
    }
};
