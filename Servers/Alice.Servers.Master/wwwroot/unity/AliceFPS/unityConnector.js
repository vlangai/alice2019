﻿AliceFPS = {

    clientId: Number,

    run: function ( clientId ) {

        this.clientId = clientId;

        if ( unityInstance !== undefined ) {

            unityInstance.Quit( function () {

                unityInstance = null;
                unityInstance = UnityLoader.instantiate( "unityContainer", "unity/AliceFPS/Build/AliceClicker.json", { onProgress: UnityProgress } );

            } );
        }
        else {

            unityInstance = UnityLoader.instantiate( "unityContainer", "unity/AliceFPS/Build/AliceFPS.json", { onProgress: UnityProgress } );
        }
    },

    connect: function () {

        console.log( "connect" );

        // browser => server : welcome

        DotNet.invokeMethodAsync( "Alice.Servers.Master", "AliceFPS_Connect", this.clientId );

        // browser => unity : welcome

        this.recv( "001," + this.clientId );
    },

    disconnect: function () {

        console.log( "disconnect" );
        DotNet.invokeMethodAsync( "Alice.Servers.Master", "AliceFPS_Disconnect", this.clientId );
    },

    send: function ( pack ) { // unity => browser

        console.log( "send: " + pack );
        DotNet.invokeMethodAsync( "Alice.Servers.Master", "AliceFPS_Recv", pack ); // client send =>  internser =>  server recv
    },

    recv: function ( pack ) { // server => browser

        console.log( "recv: " + pack );
        unityInstance.SendMessage( "WebGLHelper", "recv", pack ); // browser -> unity
    }
};
