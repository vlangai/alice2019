﻿var unityInstance;

function include( file ) {

    var js = document.createElement( "script" );
    js.type = "text/javascript";
    js.src = file;
    document.body.appendChild( js );
}

include( "unity/UnityProgress.js" );
include( "unity/UnityLoader.js" );

// --------------------------------

include( "unity/AliceClicker/unityConnector.js" );
include( "unity/AliceFPS/unityConnector.js" );

