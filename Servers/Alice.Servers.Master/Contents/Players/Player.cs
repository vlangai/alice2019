﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Contents.Players {
    public class Player {

        public long id { get; set; }
        public string userId { get; set; }
        public string nickname { get; set; } 
    }
}
