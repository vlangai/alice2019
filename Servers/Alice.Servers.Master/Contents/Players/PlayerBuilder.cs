﻿using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Contents.Players {
    public static class PlayerBuilder {

        public static void Build( ModelBuilder builder ) {

            var entity = builder.Entity<Player>();

            entity.ToTable( "player" );

            entity.HasIndex( e => e.nickname )
                .HasName( "nickname" )
                .IsUnique();

            entity.HasIndex( e => e.userId )
                .HasName( "userId" )
                .IsUnique();

            entity.Property( e => e.id )
                .HasColumnName( "id" )
                .HasColumnType( "bigint(20)" );

            entity.Property( e => e.nickname )
                .IsRequired()
                .HasColumnName( "nickname" )
                .HasColumnType( "varchar(50)" )
                .HasDefaultValueSql( "''" );

            entity.Property( e => e.userId )
                .IsRequired()
                .HasColumnName( "userId" )
                .HasColumnType( "varchar(255)" );
        }

    }
}
