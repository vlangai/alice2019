﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Servers.Master.Contents {
    public class ChatModel {

        public string channel { get; set; }
        public string message { get; set; }
    }
}
