﻿# DB Migration


1. 시작프로그램을 현재 프로젝트로 설정한다.
2. 기본 프로젝트를 현재 프로젝트로 설정한다.
3. 전체 프로젝트에서 오류가 있으면 안된다.


## Auth
```
PM> add-migration first -Context AuthDbContext
PM> update-database -Context AuthDbContext
```

## Contexts
```
PM> add-migration first -Context ContentsDbContext
PM> update-database -Context ContentsDbContext

```



# Install MariaDB at CentOS
```
$ yum install -y mariadb mariadb-server
$ systemctl enable mariadb.service
$ systemctl start mariadb.service
$ mysql_secure_installation
```

# Update MariaDB at CentOS
- Latest Version : http://yum.mariadb.org/
```
$ echo "[mariadb]" > /etc/yum.repos.d/MariaDB.repo
$ echo "name = MariaDB" >> /etc/yum.repos.d/MariaDB.repo
$ echo "baseurl = http://yum.mariadb.org/10.4.8/rhel7-amd64" >> /etc/yum.repos.d/MariaDB.repo
$ echo "gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB" >> /etc/yum.repos.d/MariaDB.repo
$ echo "gpgcheck=1" >> /etc/yum.repos.d/MariaDB.repo
```
