﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    public Text messageText;

    void Reset() {
        
        var texts = this.GetComponentsInChildren<Text>();

        foreach( var text in texts ) {

            if( text.name == "Message" ) {
                this.messageText = text;
                break;
            }
        }
    }

    public void setMessage( string text ) => this.setMessage( text, Color.white );
    public void setMessage( string text, Color color ) {

        this.messageText.text = text;
        this.messageText.color = color;
    }

}
