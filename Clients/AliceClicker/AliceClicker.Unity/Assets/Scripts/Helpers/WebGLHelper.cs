﻿#if UNITY_WEBGL
namespace AliceClicker.Helpers {
    using AliceClicker.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class WebGLHelper : SingletonBehaviour<WebGLHelper> {

        [DllImport("__Internal")] public static extern void callJs( string proejct, string packet );

        static readonly Dictionary<int,Action<string>> ResultActions = new Dictionary<int, Action<string>>();
        static int PacketID = 0;

        public void onRemoteCall( IPack pack, Action<string> onResult = null ) {


            pack.packetId = ++PacketID;

            if( onResult != null ) {

                ResultActions[ pack.packetId ] = onResult;
            }

            callJs( Application.productName, pack.pack() );
        }

        public void onResult( string packet ) {

            Debug.Log( packet );

            var rpack = packet.unpack<ResponsePack>();

            if( ResultActions.TryGetValue( rpack.packetId, out var action ) ) {

                action?.Invoke( packet );

                ResultActions.Remove( rpack.packetId );
            }
        }

        public void onNotice ( string packet ) {

            Debug.Log( packet );
        }
    }

    public static class WebGLHelperEx {

        public static void remoteCall( this MonoBehaviour self, IPack packet, Action<string> onResult = null ) {

            if( Application.isEditor ) return;
            WebGLHelper.Instance.onRemoteCall( packet, onResult );
        }
    }
}
#endif
