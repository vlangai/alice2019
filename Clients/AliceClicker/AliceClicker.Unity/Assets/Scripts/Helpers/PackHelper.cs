﻿using Newtonsoft.Json;

namespace AliceClicker.Helpers {

    public static class PackHelper {

        public static string pack<T> ( this T self ) => JsonConvert.SerializeObject( self );
        public static T unpack<T> ( this string self ) => JsonConvert.DeserializeObject<T>( self );
    }
}
