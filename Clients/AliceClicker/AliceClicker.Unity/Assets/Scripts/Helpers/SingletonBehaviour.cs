﻿namespace AliceClicker.Helpers {

    using UnityEngine;

    public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour {

        public static T Instance {

            get {
            
                if( _Instance == null ) {

                    _Instance = FindObjectOfType<T>();

                    if( _Instance == null ) {

                        var go = new GameObject( typeof(T).Name ) { hideFlags = HideFlags.HideAndDontSave };
                        _Instance = go.AddComponent<T>();
                        DontDestroyOnLoad( go );

                    }
                }

                return _Instance;
            }

        }

        private static T _Instance;
        
    }

}