﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using AliceClicker.Models;
using AliceClicker.Helpers;

public class MainController : MonoBehaviour
{

    public static MainController Instance; // singleton

    public MessageBox messageBox;

    public bool isReadyMonster;

    private void Reset() {

        if( this.messageBox == null ) {

            this.messageBox = FindObjectOfType<MessageBox>();
        }
    }

    private void Awake() {
        
        Instance = this;

        if( this.messageBox == null ) {

            Debug.LogError( new { this.messageBox } );
        }
    }

    void Start() {

        this.StartCoroutine( this.readyWorker() );
    }

    private IEnumerator readyWorker() {

        while(true){

            yield return null;

            if( this.isReadyMonster ) {

                this.messageBox.setMessage( "ready monster" );
                break;
            }
        }
    }

    public void attack( float power ) {

        var message = $"attack : {power:#,###.#0}";
        this.messageBox.setMessage( message, new Color(0f,1f,1f) );

        var attackPack = new AttackPack{ method = nameof(attack), power = power };

        this.remoteCall( attackPack, (outPacket) => {

            Debug.Log( outPacket );

            var damagePack = outPacket.unpack<DamagePack>();

            var damage = damagePack.damage;
            var exp = damagePack.exp;
            var coin = damagePack.coin;

            message = $"damage : {damage}\nexp: {exp}\ncoin: {coin}";
            this.messageBox.setMessage( message, Color.red );
        } );

    }

    public void skill ( int id ) {

        var message = $"skill : {id}";
        this.messageBox.setMessage( message, new Color( 0f, 1f, 1f ) );

        var skillPack = new SkillPack{ method = nameof(skill), id = id };

        this.remoteCall( skillPack, ( outPacket ) => {

            Debug.Log( outPacket );

            var damagePack = outPacket.unpack<DamagePack>();

            var damage = damagePack.damage;
            var exp = damagePack.exp;
            var coin = damagePack.coin;

            message = $"damage : {damage}\nexp: {exp}\ncoin: {coin}";
            this.messageBox.setMessage( message, Color.red );
        } );

    }

}
