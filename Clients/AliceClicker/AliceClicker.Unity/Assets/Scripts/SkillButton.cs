﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour {

    public int id = 1;

    void Start () {

        var button = this.GetComponent<Button>();
        if( button != null ) {

            button.GetComponentInChildren<Text>().text = $"Skill {this.id}";
            button.onClick.AddListener( () => MainController.Instance.skill( this.id ) );
        }
    }
}
