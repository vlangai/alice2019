﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    Vector3 localScale;
    Coroutine onClickCoroutine;

    void Start()
    {
        this.localScale = this.transform.localScale;

        MainController.Instance.isReadyMonster = true;
    }

    public void onClick() {

        if( this.onClickCoroutine  != null ) {

            this.StopCoroutine( this.onClickCoroutine );
            this.onClickCoroutine = null;
        }

        this.onClickCoroutine = this.StartCoroutine( this.attack() );
    }

    private IEnumerator attack() {

        var power = UnityEngine.Random.Range(1f,10f);
        this.transform.localScale += Vector3.one * power * 0.01f;

        MainController.Instance.attack( power );

        yield return new WaitForSeconds( 0.3f );

        this.transform.localScale = this.localScale;
    }
}
