mergeInto(LibraryManager.library, {

  callJs: function ( projectPtr, packetPtr ) {

	var project = Pointer_stringify( projectPtr );
	var packet = Pointer_stringify( packetPtr );

    window[ project ].request( packet );
  }

});