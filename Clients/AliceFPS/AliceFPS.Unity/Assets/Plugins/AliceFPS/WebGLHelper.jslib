mergeInto(LibraryManager.library, {

  connectJs: function ( projectPtr ) {

	var project = Pointer_stringify( projectPtr );
    window[ project ].connect();
  },

  sendJs: function ( projectPtr, packPtr ) {

	var project = Pointer_stringify( projectPtr );
	var pack = Pointer_stringify( packPtr );
    window[ project ].send( pack );
  }

});