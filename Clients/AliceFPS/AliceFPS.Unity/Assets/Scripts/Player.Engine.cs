﻿using System;
using UnityEngine;

namespace AliceFPS {

    public partial class Player {

        public class PlayerEngine {

            public Vector3 velocity { get; set; }
            public Vector3 bodyRotation { get; set; }
            public Vector3 eyeRotation { get; set; }

            public PlayerEngine ( Player player ) {

                this.player = player;
            }


            public void fixedUpdate() {

                this.move();
                this.rotation();
            }

            private void move () {

                if( this.velocity != Vector3.zero ) {

                    this.player.rb.MovePosition( this.player.rb.position + this.velocity * Time.fixedDeltaTime );
                }
            }
            private void rotation () {

                this.player.rb.MoveRotation( this.player.rb.rotation * Quaternion.Euler( this.bodyRotation )  );
                this.player.camera.transform.Rotate( this.eyeRotation );
            }

            Player player;
        }

    }
}