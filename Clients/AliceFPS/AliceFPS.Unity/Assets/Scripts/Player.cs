﻿namespace AliceFPS {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody))]
    public partial class Player : MonoBehaviour {

        [SerializeField]
        new Camera camera;
        [SerializeField]
        Rigidbody rb;
        [SerializeField]
        Collider[] colliders;


        [SerializeField]
        float moveSpeed = 5f;
        [SerializeField]
        float lookSpeed = 3f;


        void Reset () {

            this.rb = this.GetComponent<Rigidbody>();
            this.camera = this.GetComponentInChildren<Camera>();
            this.colliders = this.GetComponentsInChildren<Collider>();

        }

        private void Update () {
            
            this.input?.update();
        }

        private void FixedUpdate () {

            this.engine?.fixedUpdate();
        }

        public void setup ( bool isLocal ) {

            // local player

            if( isLocal ) {

                this.input = new PlayerInput( this );
                this.engine = new PlayerEngine( this );
            }

            // other player

            else {

                if( this.rb != null ) {

                    this.rb.isKinematic = true;
                    DestroyImmediate( this.camera );
                    this.camera = null;
                }

                // remove colliders 

            }
        }

        PlayerInput     input;
        PlayerEngine    engine;

    }
}
