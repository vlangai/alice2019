﻿using UnityEngine;

namespace AliceFPS {

    public partial class Player {

        public class PlayerInput {

            public PlayerInput ( Player player ) {

                this.player = player;
            }


            public void update() {

                // movement

                var h = Input.GetAxisRaw( "Horizontal" );
                var v = Input.GetAxisRaw( "Vertical" );

                var moveF = this.player.transform.forward * v;
                var moveR = this.player.transform.right * h;

                this.player.engine.velocity = (moveF + moveR).normalized * this.player.moveSpeed;

                // rotation

                var mx = Input.GetAxisRaw( "Mouse X" );
                var my = Input.GetAxisRaw( "Mouse Y" );

                this.player.engine.bodyRotation = new Vector3( 0f, mx, 0f ) * this.player.lookSpeed;
                this.player.engine.eyeRotation = new Vector3( -my, 0f, 0f ) * this.player.lookSpeed;

            }


            Player player;
        }

    }
}