﻿namespace AliceFPS {

    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;
    using System;

    public class NetworkTransform : MonoBehaviour {

        [SerializeField]
        private float updateCycle = 1f;

        [Header("Interpolate")]

        [SerializeField]
        private float interMoveFactor = 1f;
        [SerializeField]
        private float interRotateFactor = 1f;
        [SerializeField]
        private float interScaleFactor = 1f;

        public int netId { get; set; }
        public bool isLocal { get; set; }

        // --------------------------------------------

        private void Start () {

            this.realPos = this.transform.position;
            this.realRot = this.transform.rotation;
            this.realScale = this.transform.localScale;
        }

        private void Update () {

            // local => remote

            if( this.isLocal ) {

                this.deltaTime += Time.deltaTime;
                if( this.deltaTime >= this.updateCycle ) {

                    this.send();
                    this.deltaTime = 0f;
                }
            }

            // remote => local

            else {

                this.transform.position = Vector3.Lerp( this.transform.position, this.realPos, Time.deltaTime * this.interMoveFactor );
                this.transform.rotation = Quaternion.Slerp( this.transform.rotation, this.realRot, Time.deltaTime * this.interRotateFactor );
                this.transform.localScale = Vector3.Lerp( this.transform.localScale, this.realScale, Time.deltaTime * this.interScaleFactor );
            }
        }


        // --------------------------------------------

        public void spawn ( bool isLocal, SpawnPack pack ) {

            this.isLocal = isLocal;
            this.netId = pack.id;

            this.transform.position = pack.pos.unpack();
            this.transform.rotation = pack.rot.unpack();
            //this.transform.localScale = pack.scale.unpack();
        }


        // --------------------------------------------

        private void send () // send

            => new TransformPack {

                id = this.netId,
                pos = this.transform.position.pack(),
                rot = this.transform.rotation.pack(),
                scale = this.transform.localScale.pack(),

            }.send();

        
         public void recv ( TransformPack pack ) {

            if( this.isLocal ) return;

            this.realPos = pack.pos.unpack();
            this.realRot = pack.rot.unpack();
            this.realScale = pack.scale.unpack();
         }


        // --------------------------------------------

        private float deltaTime;

        private Vector3     realPos;
        private Quaternion  realRot;
        private Vector3     realScale;

    }
}