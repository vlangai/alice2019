﻿namespace AliceFPS {

    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    public class NetworkManager : SingletonBehaviour<NetworkManager> {

        [SerializeField]
        string serverAddress = "ws://localhost:5000";

        [SerializeField]
        NetworkTransform playerPrefab;

        [SerializeField]
        List<Transform> startPositions = new List<Transform>();

        [SerializeField]
        Button startGameButton;
        [SerializeField]
        Button exitGameButton;

        ConcurrentQueue<string> recvQueue = new ConcurrentQueue<string>();
        Dictionary<int,NetworkTransform> entities = new Dictionary<int, NetworkTransform>();

        public bool isConnected;
        public bool isObserver = true;

        void Start() {

            this.startGameButton?.gameObject.SetActive( true );
            this.exitGameButton?.gameObject.SetActive( false );

            this.startGameButton?.onClick.AddListener( this.onClickStartGameButton );
            this.exitGameButton?.onClick.AddListener( this.onClickExitGameButton );

            this.connectServer( this.serverAddress );
        }

        void Update () {

            while( this.recvQueue.TryDequeue( out var pack ) ) {

                Debug.Log( pack );

                var parameters = pack.Split(',');
                if( parameters.Length < 2 ) {

                    Debug.Log( $"error : Pack Size = {parameters.Length}" );
                    return;
                }
                if( !int.TryParse( parameters[0], out var keyInt ) ) {

                    Debug.Log( $"error : Unknown Key = {parameters[0]}" );
                    return;
                }
                var key = (Key)keyInt;

                if( !int.TryParse( parameters[1], out var senderId ) ) {

                    Debug.Log( $"error : Unknown Id = {parameters[1]}" );
                    return;
                }

                Debug.Log( new { key, senderId } );

                // update my data

                switch( key ) {

                case Key.Walcome:

                    this.onWelcome( new WalcomePack( parameters ) );

                    break;
                case Key.EnterChannel:

                    this.onEnterChannel( new EnterChannelPack( parameters ) );

                    break;
                case Key.Spawn:

                    this.onSpawn( new SpawnPack( parameters ) );

                    break;
                case Key.Transform:

                    this.onTtransform( new TransformPack( parameters ) );

                    break;
                case Key.Exit:


                    this.onExit( new ExitPack( parameters ) );
                    break;
                }
            }

        }

        private void onWelcome ( WalcomePack pack ) {

            Debug.Log( $"welcome : {pack.id}" );
            this.myId = pack.id;
            this.isConnected = true;

            new EnterChannelPack{ id=this.myId }.send();
        }
        private void onEnterChannel ( EnterChannelPack pack ) {

            this.isObserver = true;
        }
        private void onSpawn ( SpawnPack pack ) {

            var isLocal = pack.id == this.myId;

            var inst = Instantiate( this.playerPrefab );
            if( inst != null ) {

                inst.spawn( isLocal, pack );

                var player = inst.GetComponent<Player>();
                if( player != null ) {

                    player.setup( isLocal );
                }

                this.entities[pack.id] = inst;
            }

            if( isLocal ) {

                this.isObserver = false;
            }
        }
        private void onTtransform ( TransformPack pack ) {

            if( this.entities.TryGetValue( pack.id, out var entity ) ) {

                entity.recv( pack );
            }
        }
        private void onExit ( ExitPack pack ) {

            Debug.Log( $"onExit : {pack.id}" );

            if( this.entities.TryGetValue( pack.id, out var entity ) ) {

                Destroy( entity.gameObject );
                this.entities.Remove( pack.id );
            }
        }

        private void onClickStartGameButton () {

            Debug.Log( "startGame" );

            this.startGameButton?.gameObject.SetActive( false );
            this.exitGameButton?.gameObject.SetActive( true );


            this.spawnPlayer();

        }

        private void spawnPlayer () {

            // round robins

            var startPos = this.startPositions[ this.startPositionIndex ];
            ++this.startPositionIndex;
            if( this.startPositionIndex >= this.startPositions.Count ) {

                this.startPositionIndex = 0;
            }

            var pack = new SpawnPack{ id=this.myId, pos=startPos.transform.position.pack(), rot = startPos.transform.rotation.pack() };
            pack.send();

            Debug.Log( this.myId );
        }

        public void onRecv ( string pack ) => this.recvQueue.Enqueue( pack );

        private void onClickExitGameButton () {

            Debug.Log( "exitGame" );

            this.startGameButton?.gameObject.SetActive( true );
            this.exitGameButton?.gameObject.SetActive( false );
        }


        int startPositionIndex = 0;
        int myId;

    }
}