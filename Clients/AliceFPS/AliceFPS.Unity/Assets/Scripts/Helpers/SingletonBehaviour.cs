﻿namespace AliceFPS {

    using UnityEngine;

    public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour {

        public static T Instance {

            get {
            
                if( _Instance == null ) {

                    _Instance = FindObjectOfType<T>();

                    if( _Instance == null ) {

                        var go = new GameObject( typeof(T).Name ) { /*hideFlags = HideFlags.HideAndDontSave*/ };
                        _Instance = go.AddComponent<T>();

                    }
                }

                return _Instance;
            }

        }
        protected virtual void Awake () {

            if( _Instance == null ) {

                _Instance = this as T;
                DontDestroyOnLoad( this.gameObject );
            }
            else {

                DestroyImmediate( this );
            }

        }

        private static T _Instance;
        
    }


}