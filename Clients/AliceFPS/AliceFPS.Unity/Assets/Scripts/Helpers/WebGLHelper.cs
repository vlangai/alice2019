﻿#if UNITY_WEBGL && !UNITY_EDITOR
namespace AliceFPS {

    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class WebGLHelper : SingletonBehaviour<WebGLHelper> {

        [DllImport( "__Internal" )] public static extern void connectJs ( string proejct );
        [DllImport( "__Internal" )] public static extern void sendJs ( string proejct, string pack );

        public void connect () => connectJs( Application.productName );
        public void send ( string pack ) => sendJs( Application.productName, pack );
        public void recv ( string pack ) => NetworkManager.Instance.onRecv( pack );

    }

    public static class WebGLHelperEx {

        public static void connectServer ( this NetworkManager self, string serverAddress ) => WebGLHelper.Instance.connect();
        public static void send ( this ISyncPack self ) => WebGLHelper.Instance.send( self.ToString() );
   }
}
#endif
