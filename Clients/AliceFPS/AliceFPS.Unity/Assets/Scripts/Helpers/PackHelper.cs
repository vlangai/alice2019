﻿namespace AliceFPS {

    using UnityEngine;

    public static class PackHelper {

        public static Float3 pack ( this Vector3 self ) => new Float3 { x = self.x, y = self.y, z = self.z };
        public static Float4 pack ( this Quaternion self ) => new Float4 { x = self.x, y = self.y, z = self.z, w = self.w };

        public static Vector3 unpack ( this Float3 self ) => new Vector3 { x = self.x, y = self.y, z = self.z };
        public static Quaternion unpack ( this Float4 self ) => new Quaternion { x = self.x, y = self.y, z = self.z, w = self.w };

    }
}
