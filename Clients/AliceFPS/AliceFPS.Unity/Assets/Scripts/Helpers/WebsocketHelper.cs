﻿#if !UNITY_WEBGL || UNITY_EDITOR
namespace AliceFPS {

    using System.Collections;
    using UnityEngine;
    using UnityEngine.UI;
    using System;
    using System.Net.WebSockets;
    using System.Threading.Tasks;
    using System.Threading;
    using System.Text;

    public class WebsocketHelper : SingletonBehaviour<WebsocketHelper> {


        public void connect ( string serverAddress ) {

            this.serverUri =  new Uri( serverAddress );
            this.socket = new ClientWebSocket();

            Task.Run( ()=>this.worker() );
        }

        private async Task worker () {
        
            var buffer = new byte[ 1024 * 4 ];

            await this.socket.ConnectAsync( this.serverUri, CancellationToken.None );

            while( this.socket.State == WebSocketState.Open ) {

                var result = await this.socket.ReceiveAsync( new ArraySegment<byte>( buffer ), CancellationToken.None );

                if( result.MessageType == WebSocketMessageType.Text ) {

                    var pack = Encoding.UTF8.GetString( buffer, 0, result.Count );

                    Debug.Log( $"recv : {pack}" );

                    NetworkManager.Instance.onRecv( pack );
                }
                else
                if( result.MessageType == WebSocketMessageType.Close ) {

                    Debug.Log( "close" );
                }
            }
        
        }

        public void send ( string pack ) 

            => Task.Run( () 
                => this.socket.SendAsync( new ArraySegment<byte>( Encoding.UTF8.GetBytes( pack ) ), WebSocketMessageType.Text, true, CancellationToken.None ) );

        private async Task close ( int clientId ) {

            Console.WriteLine( $"close : {clientId}" );

            if( this.socket != null ) {

                if( this.socket.State == WebSocketState.Open ) {

                    await this.socket.CloseAsync( WebSocketCloseStatus.NormalClosure, "Closed by the server", CancellationToken.None );
                }
            }
        }

        private void OnApplicationQuit () {

            if( this.socket != null ) {

                this.socket.Dispose();
                this.socket = null;
            }
        }

        private Uri serverUri;
        private ClientWebSocket socket;
    }


    public static class WebsocketHelperEx {

        public static void connectServer ( this NetworkManager self, string serverAddress ) => WebsocketHelper.Instance.connect( serverAddress );

        public static void send ( this ISyncPack self ) => WebsocketHelper.Instance.send( self.ToString() );
    }
}
#endif
