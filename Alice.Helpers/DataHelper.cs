﻿using System;

namespace Alice.Helpers {

    public static class DataHelper {

        public static string plus<T>( this string self, T value ) {

            if( value is int intValue ) {

                return (Convert.ToInt32( self ) + intValue).ToString();
            }
            else
            if( value is long longValue ) {

                return (Convert.ToInt64( self ) + longValue).ToString();
            }
            else
            if( value is float floatValue ) {

                return (Convert.ToSingle( self ) + floatValue).ToString();
            }
            else
            if( value is double doubleValue ) {

                return (Convert.ToDouble( self ) + doubleValue).ToString();
            }

            return self + value.ToString();
        }
    }
}
