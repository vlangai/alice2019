using Alice.Blazor.Server.Areas.Identity;
using Alice.Blazor.Server.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;

namespace Alice.Blazor.Server {


    public class Startup {

        public Startup( IConfiguration configuration ) {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices( IServiceCollection services ) {

            services.AddDbContextPool<AuthDbContext>( options => options.UseMySql( this.Configuration.GetConnectionString( "Auth" ) ) );
            services.AddDbContextPool<ContentsDbContext>( options => options.UseMySql( this.Configuration.GetConnectionString( "Contents" ) ) );

            services.AddDefaultIdentity<IdentityUser>( options => {
                //options.SignIn.RequireConfirmedEmail=true;
            } )
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();


            //services.AddTransient<IEmailSender,EmailSender>();
            //services.Configure<EmailSetttings>( this.Configuration.GetSection( nameof(EmailSetttings) ) );

            services.AddAuthentication()
                    .AddFacebook( options => {
                        options.AppId = this.Configuration["Authentication:Facebook:AppId"];
                        options.AppSecret = this.Configuration["Authentication:Facebook:AppSecret"];
                    } )
                    .AddGoogle( options => {
                        options.ClientId = this.Configuration["Authentication:Google:ClientId"];
                        options.ClientSecret = this.Configuration["Authentication:Google:ClientSecret"];
                    } );


            services.AddMvc();
            services.AddResponseCompression( opts => {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" } );
            } );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            app.UseResponseCompression();

            if( env.IsDevelopment() ) {
                app.UseDeveloperExceptionPage();
                app.UseBlazorDebugging();
            }

            app.UseStaticFiles();
            app.UseClientSideBlazorFiles<Client.Startup>();

            app.UseRouting();

            app.UseEndpoints( endpoints => {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToClientSideBlazor<Client.Startup>( "index.html" );
            } );
        }
    }
}
