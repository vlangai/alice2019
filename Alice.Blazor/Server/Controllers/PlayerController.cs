﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Alice.Blazor.Models;
using Alice.Blazor.Server.Data;
using Microsoft.EntityFrameworkCore;

namespace Alice.Blazor.Server.Controllers {


    [ApiController]
    [Route( "api/[controller]" )]
    public class PlayerController : ControllerBase {

        private readonly ILogger<PlayerController> logger;
        private readonly ContentsDbContext dbContext;

        public PlayerController( ILogger<PlayerController> logger, ContentsDbContext dbContext ) {

            this.logger = logger;
            this.dbContext = dbContext;
        }

        // Get All
        [HttpGet]
        public async Task<IEnumerable<Player>> gets() {

            return await this.dbContext.player.ToListAsync();
        }

        // Get by Id
        [HttpGet("{id}", Name ="get")]
        public async Task<Player> get( string id ) {

            return await this.dbContext.player.FirstOrDefaultAsync( x=>x.userId == id );
        }

        // Update
        [HttpPut]
        public async Task<ActionResult> put( Player player ) {

            this.dbContext.Entry( player ).State = EntityState.Modified;
            await this.dbContext.SaveChangesAsync();
            return this.NoContent();
        }

        // Create
        [HttpPost]
        public async Task<ActionResult> post( Player player ) {

            this.dbContext.Add( player );
            await this.dbContext.SaveChangesAsync();
            return new CreatedAtRouteResult( "get", new { id=player.userId }, player );
        }

        //Delete
        [HttpDelete("{id}")]
        public async Task<ActionResult> delete( string id ) {

            var player = new Player{ userId = id };
            this.dbContext.Remove( player );
            await this.dbContext.SaveChangesAsync();
            return this.NoContent();
        }
    }
}
