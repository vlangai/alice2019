﻿using Alice.Blazor.Models;
using Alice.Blazor.Server.Data.Builders;
using Microsoft.EntityFrameworkCore;

namespace Alice.Blazor.Server.Data {
    public class ContentsDbContext : DbContext {

        public ContentsDbContext( DbContextOptions<ContentsDbContext> options ) : base( options ) {

        }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) {

            base.OnModelCreating( modelBuilder );


            PlayerBuilder.Build( modelBuilder );

        }


        public virtual DbSet<Player> player { get; set; }
    }
}
