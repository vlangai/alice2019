﻿using Alice.Blazor.Models;
using Microsoft.EntityFrameworkCore;

namespace Alice.Blazor.Server.Data.Builders {
    public static class PlayerBuilder {

        public static void Build( ModelBuilder builder ) {

            var entity = builder.Entity<Player>();

            entity.HasKey( e => e.userId )
                .HasName( "PRIMARY" );

            entity.ToTable( "player" );

            entity.HasIndex( e => e.nickName )
                .HasName( "nick_name" )
                .IsUnique();

            entity.Property( e => e.userId )
                .HasColumnName( "user_id" )
                .HasColumnType( "varchar(255)" );

            entity.Property( e => e.nickName )
                .IsRequired()
                .HasColumnName( "nick_name" )
                .HasColumnType( "varchar(50)" )
                .HasDefaultValueSql( "''" );
        }

    }
}
