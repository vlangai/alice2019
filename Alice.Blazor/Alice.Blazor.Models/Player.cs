﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Alice.Blazor.Models {

    public class Player {

        [Required]
        public string userId { get; set; }
        [Required]
        public string nickName { get; set; } 
    }
}
