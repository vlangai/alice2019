﻿using Alice.Blazor.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Alice.Blazor.Client.Pages.Players {
    public partial class Create : ComponentBase {

        [Inject] HttpClient http {get; set;}
        [Inject] NavigationManager navigation { get; set; }

        [Parameter] public string userId { get; set; }

        private Player player {get; set;} = new Player{ userId=Guid.NewGuid().ToString() };


        private async Task create() {

            await this.http.PostJsonAsync( "/api/player", this.player );
            this.navigation.NavigateTo( "/players" );
        }

    }
}
