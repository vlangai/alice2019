﻿using Alice.Blazor.Models;
using Microsoft.AspNetCore.Components;

namespace Alice.Blazor.Client.Pages.Players {
    public partial class Form {

        [Parameter] public Player player { get; set; }
        [Parameter] public string buttonText { get; set; } = "Save";
        [Parameter] public EventCallback onValidSubmit { get; set; }

    }
}
