﻿using Alice.Blazor.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Alice.Blazor.Client.Pages.Players {
    public partial class Index {

        [Inject] HttpClient http { get; set; }
        [Inject] IJSRuntime js {get; set; }

        private Player[] players;

        protected override async Task OnInitializedAsync() {

            await this.loadPlayers();
        }

        private async Task delete( string userId ) {

            var selectedPlayer = this.players.FirstOrDefault( x=>x.userId == userId );

            var ok = await this.js.InvokeAsync<bool>( "confirm", $"Do you want to delete '{selectedPlayer.nickName}'?"  );
            if( ok ) {

                await this.http.DeleteAsync( $"/api/player/{userId}" );
                await this.loadPlayers();
            }
        }

        private async Task loadPlayers() => this.players = await http.GetJsonAsync<Player[]>( "/api/player" );

    }
}
