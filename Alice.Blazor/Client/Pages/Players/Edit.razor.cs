﻿using Alice.Blazor.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Alice.Blazor.Client.Pages.Players {
    public partial class Edit : ComponentBase {

        [Inject] HttpClient http {get; set;}
        [Inject] NavigationManager navigation { get; set; }

        [Parameter] public string userId { get; set; }

        private Player player {get; set;} = new Player();


        protected override async Task OnParametersSetAsync() {

            this.player = await this.http.GetJsonAsync<Player>( $"/api/player/{this.userId}" );


        }

        private async Task update() {

            await this.http.PutJsonAsync( "/api/player", this.player );

            this.navigation.NavigateTo( "/players" );
        }

    }
}
